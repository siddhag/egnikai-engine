# Use an official Java runtime as a parent image
FROM openjdk:8

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD ./target /app
ADD ./bin/start.sh /app
ADD ./sshKey /app/ssh
ADD ./lib /app/lib
ADD ./scripts /app/scripts

RUN useradd -ms /bin/bash  egnikaiusr
# Install any needed packages specified in requirements.txt
# RUN pip install -r requirements.txt

# Make port 8080 available to the world outside this container
# EXPOSE 8080

RUN ["chmod", "555", "start.sh"]
RUN ["chmod", "-R", "555", "/app/scripts"]

# Run our app when the container launches
CMD ["./start.sh", "."]