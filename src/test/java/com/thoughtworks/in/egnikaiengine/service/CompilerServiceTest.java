package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.dto.CompileRequestDTO;
import com.thoughtworks.in.egnikaiengine.dto.RunningMessageDTO;
import com.thoughtworks.in.egnikaiengine.utils.ScriptUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CompilerServiceTest {

    private RunningMessageDTO runningMessageDTO;
    private CompileRequestDTO compileRequestDTO;

    @Mock
    private OperationService operationService;

    @Mock
    private ScriptUtils scriptUtils;


    private CompilerService compilerService;

    @Before
    public void setUp() {

        compilerService = new CompilerService(operationService, scriptUtils);

        runningMessageDTO = new RunningMessageDTO();
        runningMessageDTO.setCandidateId(4);
        runningMessageDTO.setOperationId(1);
        runningMessageDTO.setPlatform("JAVA");
        runningMessageDTO.setProblemId(1);

        compileRequestDTO = new CompileRequestDTO("src", runningMessageDTO.getCandidateId(), runningMessageDTO.getOperationId(), runningMessageDTO.getProblemId());
    }


    @Test(expected = Exception.class)
    public void shouldThrowNullPointerException() {
        compilerService.compile(null);

        verify(operationService).updateCompileStatusToDB(1, "FAILED", "error");

    }
}