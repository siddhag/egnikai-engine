package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.dto.CloningMessageDTO;
import com.thoughtworks.in.egnikaiengine.dto.SavingMessageDTO;
import com.thoughtworks.in.egnikaiengine.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikaiengine.persistence.model.*;
import com.thoughtworks.in.egnikaiengine.persistence.repository.CandidateRepository;
import com.thoughtworks.in.egnikaiengine.persistence.repository.ProblemRepository;
import com.thoughtworks.in.egnikaiengine.utils.GitUtils;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class CandidateServiceTest {

    @Mock
    private GitUtils gitUtils;

    @Mock
    private CandidateRepository candidateRepository;

    @Mock
    private ProblemService problemService;

    @Mock
    private OperationService operationService;

    private CandidateService candidateService;

    private  CloningMessageDTO cloningMessageDTO;
    private  Operation operation;

    @Before
    public void setUp() {
        cloningMessageDTO = new CloningMessageDTO();
        cloningMessageDTO.setCandidateId(1);
        cloningMessageDTO.setOperationId(1);
        cloningMessageDTO.setGitURL("gitUrl");

        candidateService = new CandidateService(gitUtils, operationService, candidateRepository, problemService, "/code", "c");

        ReflectionTestUtils.setField(this.candidateService,"candidatePrefix", "c");
        ReflectionTestUtils.setField(this.candidateService,"codeDirectoryPrefix", "/code");

    }

    @Test
    public void verifySetUpEnvironment() throws GitAPIException, IOException {

        operation = new Operation();
        operation.setOperationName(OperationName.CLONE.name());
        operation.setOperationStatus(OperationStatus.IN_PROGRESS.name());
        operation.setOperationId(1);

        when(candidateRepository.findById(1l)).thenReturn(Optional.of(getCandidate()));

        candidateService.setupEnvironmentForCandidate(cloningMessageDTO);


        Mockito.verify(gitUtils).cloneAndSwitchTo("gitUrl", candidateService.getCandidateFolder(cloningMessageDTO.getCandidateId()), "c1");

        ArgumentCaptor<Candidate> candidateArgumentCaptor = ArgumentCaptor.forClass(Candidate.class);

        Mockito.verify(candidateRepository).save(candidateArgumentCaptor.capture());

        Candidate savedCandidate = candidateArgumentCaptor.getValue();
        Assert.assertEquals(1, savedCandidate.getCandidateId());
        Assert.assertEquals("c1", savedCandidate.getCandidateGitBranch());
    }


    @Test(expected = NullPointerException.class)
    public void shouldThrowAnExceptionWhenDTOIsNull() {
        candidateService.setupEnvironmentForCandidate(null);
    }

    @Test
    public void verifySaveProblem() throws GitAPIException {
        SavingMessageDTO savingMessageDTO = new SavingMessageDTO();
        savingMessageDTO.setCandidateId(1);
        savingMessageDTO.setFilePath("problem/file/path");
        savingMessageDTO.setTempFilePath("temp/path");


        candidateService.saveProblem(savingMessageDTO);

        Mockito.verify(gitUtils).commitAndPush(candidateService.getCandidateFolder(savingMessageDTO.getCandidateId()), "save file");
    }

    @Test
    public void shouldAddProblemToAttemptedProblemList() throws ResourceNotFoundException {
        Operation operationWithId1 = getCandidateWithNoAttemptedProblem();
        Assert.assertEquals(operationWithId1.getCandidate().getAttemptedProblems().size(), 0);

        when(operationService.getOperation(1)).thenReturn(operationWithId1);

        candidateService.updateAttemptedProblemInDB(1, 1);

        Assert.assertEquals(operationWithId1.getCandidate().getAttemptedProblems().size(), 1);



    }


    @Test
    public void shouldMarkFailedIfMarkedAsPassedEarlier() throws ResourceNotFoundException {
        Operation operationWithId1 = getCandidateWithPassingAttemptedProblem();
        Assert.assertEquals(operationWithId1.getCandidate().getAttemptedProblems().get(0).isPassed(), false);


        when(operationService.getOperation(1)).thenReturn(operationWithId1);

        candidateService.updateAttemptedProblemInDB(1, 1);

        Assert.assertEquals(true, operationWithId1.getCandidate().getAttemptedProblems().get(0).isPassed());
    }

    @Test
    public void shouldMarkPassedIfNotMarkedAsPassedEarlier() throws ResourceNotFoundException {

        Operation operationWithId1 = getCandidateWithFailedAttemptedProblem();
        Assert.assertEquals(operationWithId1.getCandidate().getAttemptedProblems().get(0).isPassed(), true);


        when(operationService.getOperation(1)).thenReturn(operationWithId1);

        candidateService.updateAttemptedProblemInDB(1, 1);

        Assert.assertEquals(false, operationWithId1.getCandidate().getAttemptedProblems().get(0).isPassed());

    }

    private Operation getCandidateWithNoAttemptedProblem(){
        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        candidate.setAttemptedProblems(new ArrayList<>());

        Operation operation = new Operation();
        operation.setOperationId(1);
        operation.setCandidate(candidate);

        return operation;

    }


    private Operation getCandidateWithPassingAttemptedProblem(){
        AttemptedProblem attemptedProblem = new AttemptedProblem();
        attemptedProblem.setProblem(getProblem());
        attemptedProblem.setPassed(false);

        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        candidate.setAttemptedProblems(Arrays.asList(attemptedProblem));

        Operation operation = new Operation();
        operation.setOperationId(1);
        operation.setCandidate(candidate);
        operation.setOperationStatus(OperationStatus.COMPLETED.name());

        return operation;

    }

    private Operation getCandidateWithFailedAttemptedProblem(){
        AttemptedProblem attemptedProblem = new AttemptedProblem();
        attemptedProblem.setProblem(getProblem());
        attemptedProblem.setPassed(true);

        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        candidate.setAttemptedProblems(Arrays.asList(attemptedProblem));

        Operation operation = new Operation();
        operation.setOperationId(1);
        operation.setCandidate(candidate);
        operation.setOperationStatus(OperationStatus.FAILED.name());

        return operation;

    }


    private Problem getProblem(){
        Problem problem = new Problem();
        problem.setProblemId(1);
        return problem;
    }

    private Candidate getCandidate(){

        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        return candidate;
    }


}