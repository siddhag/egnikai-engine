package com.thoughtworks.in.egnikaiengine.utils;

import com.thoughtworks.in.egnikaiengine.dto.ProcessOutput;
import javafx.scene.effect.Reflection;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


@RunWith(MockitoJUnitRunner.class)
public class ScriptUtilsTest {

    private ScriptUtils scriptUtils;

    @Mock
    private Process process;

    @Before
    public void setUp() {
        scriptUtils = new ScriptUtils();

        ReflectionTestUtils.setField(scriptUtils,"egnikaiUser", "egnikaiUser");
    }




    @Test
    public void shouldInvokeRunScript() throws InterruptedException, IOException, TimeoutException {

        ProcessOutput processOutput = scriptUtils.runScript("compile", 20000, String.valueOf(4), String.valueOf(12));
        Assert.assertEquals(1,processOutput.getExitStatus());
    }

    @Test(expected = Exception.class)
    public void shouldThrowAnException() throws IOException, TimeoutException, InterruptedException {

    Mockito.doThrow(new TimeoutException()).when(process).getInputStream();

    scriptUtils.runScript("compile", 20000, String.valueOf(4), String.valueOf(12));

    }


}