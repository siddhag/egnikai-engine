package com.thoughtworks.in.egnikaiengine.controller;

import com.thoughtworks.in.egnikaiengine.dto.CloningMessageDTO;
import com.thoughtworks.in.egnikaiengine.service.CandidateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CloningQueueMessageReceiverTest {

    @Mock
    private CandidateService candidateService;

    @InjectMocks
    private CloningQueueMessageReceiver cloningQueueMessageReceiver;

    @Test
    public void shouldCallCandidateService_EnvironmentSetupWhenMessageReceived() {
        CloningMessageDTO cloningMessageDTO = new CloningMessageDTO();
        cloningMessageDTO.setCandidateId(1);
        cloningMessageDTO.setGitURL("gitUrl");
        cloningMessageDTO.setOperationId(1);
        cloningQueueMessageReceiver.receivedMessage(cloningMessageDTO);

        Mockito.verify(candidateService).setupEnvironmentForCandidate(cloningMessageDTO);
    }

    @Test
    public void shouldNotThrowAnExceptionWhenDTOIsNull() {
        cloningQueueMessageReceiver.receivedMessage(null);
    }

}