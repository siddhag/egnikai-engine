package com.thoughtworks.in.egnikaiengine.utils;

import com.thoughtworks.in.egnikaiengine.dto.ProcessOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Component
public class ScriptUtils {
    private static final Logger logger = LoggerFactory.getLogger(ScriptUtils.class);

    @Value("${egnikai.system.user}")
    private String egnikaiUser;


    public ProcessOutput runScript(String scriptName, long timeoutInMillis, String... args) throws InterruptedException, TimeoutException, IOException {
        ProcessBuilder processBuilder = runScriptAsEgnikaiUser(scriptName, args);
        Process process = processBuilder.start();
        if (process.waitFor(timeoutInMillis, TimeUnit.MILLISECONDS)) {
            String output = getOutput(process.getInputStream());
            String error = getOutput(process.getErrorStream());
            return new ProcessOutput(output, error, process.exitValue(), false);
        } else {
            throw new TimeoutException();
        }
    }


    public ProcessBuilder runScriptAsEgnikaiUser(String scriptName, String... args) {
        String scriptFileName = "/app/scripts/" + scriptName + ".sh";
        logger.debug("Running script:- {} ,as user {}", scriptFileName, egnikaiUser);
        String[] formattedArgs = formatAndEscapeArgs(args);
        return new ProcessBuilder("su", "-s", "/bin/sh", egnikaiUser, "-c", scriptFileName + " " + String.join(" ", formattedArgs));
    }

    private String[] formatAndEscapeArgs(String[] args) {
        return Arrays.stream(args)
                .map(arg -> arg.replace("\\", "\\\\"))//escaping \ with \\
                .map(arg -> arg.replace("\"", "\\\""))// escaping " with \"
                .map(arg -> '"' + arg + '"') // wrap the command in quotes
                .toArray(String[]::new);
    }

    private String getOutput(InputStream inputStream) throws IOException {

        try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader reader = new BufferedReader(inputStreamReader)) {

            StringBuilder builder = new StringBuilder();

            for (String line = null; (line = reader.readLine()) != null; ) {
                builder.append(line);
                builder.append(System.getProperty("line.separator"));
            }
            String result = builder.toString();

            return result;
        }
    }
}
