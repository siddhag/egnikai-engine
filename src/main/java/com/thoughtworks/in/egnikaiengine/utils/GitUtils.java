package com.thoughtworks.in.egnikaiengine.utils;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;

/**
 * Git Utility Methods
 */
@Component
public class GitUtils {

    private static final Logger logger = LoggerFactory.getLogger(GitUtils.class);

    private SshSessionFactory sshSessionFactory;

    @Autowired
    public GitUtils(SshSessionFactory sshSessionFactory) {
        this.sshSessionFactory = sshSessionFactory;
    }

    private Git cloneRepository(final String remoteUrl, final Path localPath) throws IOException {
        logger.debug("Cloning the repository");
        FileUtils.deleteDirectory(localPath.toFile());

        try {
            return Git.cloneRepository()
                    .setURI(remoteUrl)
                    .setTransportConfigCallback(transport -> {
                        SshTransport sshTransport = (SshTransport) transport;
                        sshTransport.setSshSessionFactory(sshSessionFactory);
                    })
                    .setDirectory(localPath.toFile())
                    .call();
        } catch (GitAPIException e) {
            throw new IllegalStateException(e);
        }
    }


    public void cloneAndSwitchTo(String url, File localDirectoryPath, String branchName) throws GitAPIException, IOException {
        Git git = cloneRepository(url, localDirectoryPath.toPath());
        logger.debug("Cloning complete");
        boolean branchExists = isBranchExists(branchName, git);
        logger.debug("Switching to branch: " + branchName);
        if (!branchExists) {
            CreateBranchCommand bcc = git.branchCreate();
            bcc.setName(branchName)
                    .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                    .setStartPoint("origin/master")
                    .setForce(true)
                    .call();
        }

        CheckoutCommand checkout = git.checkout().setName(branchName);
        if (branchExists) {
            checkout = checkout.setCreateBranch(true).
                    setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM).
                    setStartPoint("origin/" + branchName);
        }
        checkout.call();
    }


    private boolean isBranchExists(String branchName, Git git) throws GitAPIException {
        List<Ref> listRefsBranches = git.branchList().setListMode(ListBranchCommand.ListMode.ALL).call();
        for (Ref refBranch : listRefsBranches) {
            logger.debug("Branch name: " + refBranch.getName());
            if (("refs/remotes/origin/" + branchName).equals(refBranch.getName())) {
                return true;
            }
        }
        return false;
    }


    public void commitAndPush(File localPath, String commitMessage) throws GitAPIException {

        try (Git git = Git.init()
                .setDirectory(localPath)
                .call()) {


            AddCommand ac = git.add();
            ac.addFilepattern(".");
            ac.call();

            //TODO find the committer id
            CommitCommand commit = git.commit();
            commit.setCommitter("Egnikai", "egnikai@thoughtworks.com")
                    .setMessage(commitMessage);
            commit.call();


            PushCommand pc = git.push();
            pc.setTransportConfigCallback(transport -> {
                SshTransport sshTransport = (SshTransport) transport;
                sshTransport.setSshSessionFactory(sshSessionFactory);
            });

            try {
                Iterator<PushResult> it = pc.call().iterator();
                if (it.hasNext()) {
                    logger.debug("Pushing" + it.next().toString());
                }
                logger.info("Saving completed at location: " + localPath + "with commit message: " + commitMessage);
            } catch (InvalidRemoteException e) {
                logger.error(e.getLocalizedMessage(), e);
            }

        }

    }

}
