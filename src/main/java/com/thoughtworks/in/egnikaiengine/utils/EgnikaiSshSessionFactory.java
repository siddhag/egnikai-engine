package com.thoughtworks.in.egnikaiengine.utils;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.util.FS;
import org.springframework.beans.factory.annotation.Value;

import java.nio.file.Path;

public class EgnikaiSshSessionFactory extends JschConfigSessionFactory {
    private final Path privateKey;
    private final String passphrase;

    @Value("${egnikai.engine.ssh.known_hosts_path}")
    private String sshKnownHostsPath;

    public EgnikaiSshSessionFactory(Path privateKey, String passphrase) {
        this.privateKey = privateKey;
        this.passphrase = passphrase;
    }

    @Override
    protected void configure(OpenSshConfig.Host host, Session session) {
        session.setUserInfo(new PassphraseUserInfo(passphrase));
        session.setConfig("StrictHostKeyChecking", "no");
        //TODO Remove security flaw
    }

    @Override
    protected JSch createDefaultJSch(FS fs) throws JSchException {
        if (privateKey != null) {
            JSch defaultJSch = super.createDefaultJSch(fs);
            defaultJSch.addIdentity(privateKey.toFile().getAbsolutePath());
            defaultJSch.setKnownHosts(sshKnownHostsPath);
            return defaultJSch;
        } else {
            return super.createDefaultJSch(fs);
        }
    }
}
