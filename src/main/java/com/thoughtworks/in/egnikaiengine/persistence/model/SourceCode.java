package com.thoughtworks.in.egnikaiengine.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "Source")
@EqualsAndHashCode
@ToString
public class SourceCode {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected long sourceId;

    /**
     * SourceCode file name
     */
    @Getter
    @Setter
    private String sourceName;

    /**
     * Relative file path
     */
    @Getter
    @Setter
    private String filePath;

    /**
     * SourceCode class name
     */
    @Getter
    @Setter
    private String className;

    /**
     * Whether candidate is allowed to edit this source code or not
     */
    @Getter
    @Setter
    private boolean isEditable;
}
