package com.thoughtworks.in.egnikaiengine.persistence.model;

import java.io.Serializable;

/**
 * Created by siddhag on 16/06/17.
 */
public enum TestType implements Serializable {
    PRIVATE, PUBLIC
}
