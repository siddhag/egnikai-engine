package com.thoughtworks.in.egnikaiengine.persistence.repository;

import com.thoughtworks.in.egnikaiengine.persistence.model.Candidate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface CandidateRepository extends CrudRepository<Candidate, Long> {
    Candidate findByCandidateId(long candidateId);
}
