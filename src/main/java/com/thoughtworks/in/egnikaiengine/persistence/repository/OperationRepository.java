package com.thoughtworks.in.egnikaiengine.persistence.repository;

import com.thoughtworks.in.egnikaiengine.persistence.model.Operation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationRepository extends CrudRepository<Operation, String> {

    Operation findByOperationId(long operationId);

}
