package com.thoughtworks.in.egnikaiengine.persistence.model;

public enum OperationName {
    CLONE,
    SAVE,
    COMPILE,
    RUN
}
