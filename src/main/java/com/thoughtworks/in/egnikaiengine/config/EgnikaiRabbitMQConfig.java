package com.thoughtworks.in.egnikaiengine.config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EgnikaiRabbitMQConfig {
    @Value("${egnikai.engine.topic-exchange}")
    private String topicExchangeName;

    @Value("${egnikai.engine.queue.cloning}")
    private String cloningQueueName;

    @Value("${egnikai.engine.routing.cloning}")
    private String cloningRoutingKey;

    @Value("${egnikai.engine.queue.saving}")
    private String savingQueueName;

    @Value("${egnikai.engine.routing.saving}")
    private String savingRoutingKey;

    @Value("${egnikai.engine.queue.running}")
    private String runningQueueName;

    @Value("${egnikai.engine.routing.running}")
    private String runningRoutingKey;

    @Bean
    public Jackson2JsonMessageConverter jsonMessageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

    @Bean
    public SimpleRabbitListenerContainerFactory jsaFactory(ConnectionFactory connectionFactory,
                                                           SimpleRabbitListenerContainerFactoryConfigurer configurer) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        setupTopicExchangeAndQueues(connectionFactory);


        configurer.configure(factory, connectionFactory);
        factory.setMessageConverter(jsonMessageConverter());
        return factory;
    }

    private void setupTopicExchangeAndQueues(ConnectionFactory connectionFactory) {
        RabbitAdmin admin = new RabbitAdmin(connectionFactory);
        TopicExchange exchange = new TopicExchange(topicExchangeName);
        admin.declareExchange(exchange);
        Queue cloningQueue = new Queue(cloningQueueName);
        admin.declareQueue(cloningQueue);
        admin.declareBinding(
                BindingBuilder.bind(cloningQueue).to(exchange).with(cloningRoutingKey));


        Queue savingQueue = new Queue(savingQueueName);
        admin.declareQueue(savingQueue);
        admin.declareBinding(
                BindingBuilder.bind(savingQueue).to(exchange).with(savingRoutingKey));

        Queue runningQueue = new Queue(runningQueueName);
        admin.declareQueue(runningQueue);
        admin.declareBinding(
                BindingBuilder.bind(runningQueue).to(exchange).with(runningRoutingKey));
    }

}
