package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikaiengine.persistence.model.Problem;
import com.thoughtworks.in.egnikaiengine.persistence.repository.ProblemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProblemService {
    private ProblemRepository problemRepository;

    private static final Logger logger = LoggerFactory.getLogger(ProblemService.class);

    @Autowired
    public ProblemService(ProblemRepository problemRepository) {
        this.problemRepository = problemRepository;
    }

    Problem getProblem(long problemId) throws ResourceNotFoundException {
        Problem problem = problemRepository.findByProblemId(problemId);
        if(problem == null){
            logger.error("Problem not found: "+ problemId);
            throw new ResourceNotFoundException("Problem with Id "+problemId+" not found");
        }
        return problem;
    }
}
