package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikaiengine.persistence.model.Operation;
import com.thoughtworks.in.egnikaiengine.persistence.model.OperationName;
import com.thoughtworks.in.egnikaiengine.persistence.model.OperationStatus;
import com.thoughtworks.in.egnikaiengine.persistence.repository.OperationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperationService {

    private static final Logger logger = LoggerFactory.getLogger(OperationService.class);
    private OperationRepository operationRepository;


    @Autowired
    public OperationService(OperationRepository operationRepository) {
        this.operationRepository = operationRepository;
    }

    Operation updateRunStatusToDB(long operationId, String operationStatus, String result) {
        return updateStatusToDB(operationId, OperationName.RUN.toString(), operationStatus, result);
    }

    Operation updateCompileStatusToDB(long operationId, String operationStatus, String result) {
        return updateStatusToDB(operationId, OperationName.COMPILE.toString(), operationStatus, result);
    }

    private Operation updateStatusToDB(long operationId, String operationName, String status, String result) {
        Operation operation = null;
        try {
            operation = operationRepository.findByOperationId(operationId);
            operation.setErrorInformation(result);
            operation.setOperationName(operationName);
            operation.setOperationStatus(status);
            operationRepository.save(operation);
        } catch (Exception exception) {
            logger.error("Updating operation status failed for operation Id:" + operationId + " with exception " + exception);
        }
        return operation;
    }

    Operation writeCloneStatusToDB(long operationId, OperationStatus operationStatus) {
        Operation operation = null;
        try {
            operation = operationRepository.findByOperationId(operationId);
            operation.setOperationStatus(operationStatus.name());
            operationRepository.save(operation);
        } catch (Exception exception) {
            logger.error("Updating operation status failed for operation Id:" + operationId + " with exception " + exception);
        }

        return operation;
    }

    Operation getOperation(long operationId) throws ResourceNotFoundException {
        Operation operation = operationRepository.findByOperationId(operationId);
        if (operation == null) {
            logger.error("Operation not found: " + operationId);
            throw new ResourceNotFoundException("Operation not found: " + operationId);
        }
        return operation;
    }

}
