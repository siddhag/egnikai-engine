package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.persistence.model.AttemptedProblem;
import com.thoughtworks.in.egnikaiengine.persistence.model.Candidate;
import com.thoughtworks.in.egnikaiengine.persistence.repository.CandidateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScoreService {

    private static final Logger logger = LoggerFactory.getLogger(ScoreService.class);

    private CandidateRepository candidateRepository;

    @Autowired
    public ScoreService(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    public void calculateScore(long candidateId) {
        try {
            logger.debug("Calculating and Updating candidate score...");
            Candidate candidate = candidateRepository.findByCandidateId(candidateId);

            List<AttemptedProblem> attemptedProblems = candidate.getAttemptedProblems();

            int score = attemptedProblems.stream()
                    .filter(attemptedProblem -> attemptedProblem.isPassed())
                    .mapToInt(attemptedProblem -> attemptedProblem.getProblem().getCredit())
                    .sum();

            candidate.setScore(score);
            candidateRepository.save(candidate);
            logger.debug("Candidate score updated successfully. "+score+" for candidate " +candidate);
        } catch (Exception e) {
            logger.error("Failed to update score for candidate ",candidateId, e);
        }
    }
}

