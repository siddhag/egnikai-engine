package com.thoughtworks.in.egnikaiengine.controller;

import com.thoughtworks.in.egnikaiengine.dto.SavingMessageDTO;
import com.thoughtworks.in.egnikaiengine.service.CandidateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SavingQueueMessageReceiver {

    private static final Logger logger = LoggerFactory.getLogger(SavingQueueMessageReceiver.class);

    private CandidateService candidateService;

    @Autowired
    public SavingQueueMessageReceiver(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @RabbitListener(queues = "${egnikai.engine.queue.saving}", containerFactory = "jsaFactory")
    public void receivedMessage(SavingMessageDTO savingMessageDTO) {


        try {
            logger.info("Save operation started for candidate", savingMessageDTO.getCandidateId());
            candidateService.saveProblem(savingMessageDTO);
            logger.info("Save operation complete for candidate", savingMessageDTO.getCandidateId());
        } catch (Exception exception) {
            logger.info("Save operation failed for", savingMessageDTO + "\n with exception " + exception);
        }

    }

}
