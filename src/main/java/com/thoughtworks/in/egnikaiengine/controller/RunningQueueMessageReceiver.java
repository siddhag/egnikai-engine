package com.thoughtworks.in.egnikaiengine.controller;

import com.thoughtworks.in.egnikaiengine.dto.RunningMessageDTO;
import com.thoughtworks.in.egnikaiengine.service.RunOrchestrator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RunningQueueMessageReceiver {
    private static final Logger logger = LoggerFactory.getLogger(RunningQueueMessageReceiver.class);

    private RunOrchestrator runOrchestrator;

    @Autowired
    public RunningQueueMessageReceiver(RunOrchestrator runOrchestrator) {
        this.runOrchestrator = runOrchestrator;
    }

    @RabbitListener(queues = "${egnikai.engine.queue.running}", containerFactory = "jsaFactory")
    public void receivedMessage(RunningMessageDTO runningMessageDTO) {
        try {
            logger.info("Execution delegated to RunOrchestrator for Problem " + runningMessageDTO.getProblemId() + " of candidate" + runningMessageDTO.getCandidateId());
            runOrchestrator.compileAndRun(runningMessageDTO);
            logger.info("Problem execution successful for Problem " + runningMessageDTO.getProblemId() + " of candidate" + runningMessageDTO.getCandidateId());
        } catch (Exception exception) {
            logger.error("Run operation failed for " + runningMessageDTO + "\n with exception" + exception);
        }
    }
}
