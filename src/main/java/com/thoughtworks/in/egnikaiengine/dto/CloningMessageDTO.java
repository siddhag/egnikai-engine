package com.thoughtworks.in.egnikaiengine.dto;

import lombok.Data;

@Data
public class CloningMessageDTO {
    private String gitURL;
    private long candidateId;
    private long operationId;

}
