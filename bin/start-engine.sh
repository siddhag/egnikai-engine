#!/usr/bin/env bash

EGNIKAI_RABBIT_MQ_USERID="egnikaimq"
EGNIKAI_RABBIT_MQ_PASSWORD=`pass egnikai/rmq`
EGNIKAI_DB_PASSWORD=`pass egnikai/dbroot`
EGNIKAI_ENGINE_CONTAINER_NAME="egnikai-engine"
EGNIKAI_ENGINE_IMAGE_NAME="egnikai-engine-image"
EGNIKAI_DB_CONTAINER_NAME="egnikaidb"
EGNIKAI_RMQ_CONTAINER_NAME="egnikaimq"
EGNIKAI_SSH_KEY_PATH="/app/ssh/id_rsa"

egnikai_ds_url="jdbc:mysql://egnikaidb:3306/egnikaidb"

echo "Stopping existing containers..."
sudo docker stop ${EGNIKAI_ENGINE_CONTAINER_NAME}

echo "Removing existing containers and images..."
sudo docker rm ${EGNIKAI_ENGINE_CONTAINER_NAME}
sudo docker rmi ${EGNIKAI_ENGINE_IMAGE_NAME}

echo "Building engine docker image..."
sudo docker build -t ${EGNIKAI_ENGINE_IMAGE_NAME} .

echo "Starting Egnikai Engine..."

sudo docker run -v sharedCodeVolume:/code -it -e "EGNIKAI_DS_URL=$egnikai_ds_url" -e "EGNIKAI_SSH_KEY_PATH=$EGNIKAI_SSH_KEY_PATH" -e "EGNIKAI_RABBIT_MQ_PASSWORD=$EGNIKAI_RABBIT_MQ_PASSWORD" -e "EGNIKAI_DB_PASSWORD=$EGNIKAI_DB_PASSWORD" --link ${EGNIKAI_DB_CONTAINER_NAME} --link ${EGNIKAI_RMQ_CONTAINER_NAME} --name ${EGNIKAI_ENGINE_CONTAINER_NAME} ${EGNIKAI_ENGINE_IMAGE_NAME}

