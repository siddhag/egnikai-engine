#!/usr/bin/env bash

# Use this script to compile and build the project.

EGNIKAI_DB_CONTAINER_NAME="egnikaidb"
EGNIKAI_RMQ_CONTAINER_NAME="egnikaimq"

datestr=`date +"%a_%b_%d_%Y"`


echo "backing up /etc/hosts..."
sudo cp /etc/hosts /etc/hosts-bkup-$datestr
echo "adding /etc/host entry..."
cp /etc/hosts ./hosts
echo "127.0.0.1        $EGNIKAI_DB_CONTAINER_NAME" >> ./hosts
echo "127.0.0.1        $EGNIKAI_RMQ_CONTAINER_NAME" >> ./hosts
sudo cp ./hosts /etc/hosts
rm ./hosts
echo "running build..."
mvn clean compile package -Dspring.datasource.password=`pass egnikai/dbroot` -Dspring.rabbitmq.password=`pass egnikai/rmq`  -Degnikai.engine.git.password=`pass egnikai/git` -Degnikai.engine.ssh.key_path="~/.ssh" -Dspring.datasource.url="jdbc:mysql://egnikaidb:3306/egnikaidb"

echo "cleaning up /etc/hosts..."
sudo mv /etc/hosts-bkup-$datestr /etc/hosts
sudo rm /etc/hosts-bkup-$datestr
echo "[DONE]"
